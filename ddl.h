//header for ddl.c
//William Doyle
//December 27th 2019


#define _TYPE_ int 

struct ddl_node {
	ddl_node * prev;
	ddl_node *next;
	_TYPE_ data;
};

struct ddl{
	struct ddl_node * top;
	int cursor;
	int size;
	void (*remove_node)(struct ddl* , int);		//remove node at pos
	void (*clear)(struct ddl*);	 		//free all nodes
	void (*add_at)(struct ddl*, int, int);		//add node at provided pos
	void (*add)(struct ddl * , _TYPE_);		//add node at end -- will call add at
	_TYPE_ (*get_at)(struct ddl* , int);	  	//return the value at position
	_TYPE_(*next)(struct ddl*);			//return next value based on cursor... move cursor
};	

void PROTO_remove_node(struct ddl* , int);
void PROTO_clear(struct ddl* );
void PROTO_add_at(struct ddl * , int, _TYPE_);
void PROTO_add(struct ddl *  , _TYPE_);
_TYPE_ PROTO_get_at(struct ddl * , int);
_TYPE_ PROTO_next(struct ddl *);

struct ddl *  new_ddl(int);		//create a new DDL and set the function pointers to corisponding prototypes
