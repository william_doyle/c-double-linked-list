//William Doyle
//a double linked list made in C 
//December 27th 2019
//ddl.c
//Practice C programming

/* Includes */
#include "ddl.h"


void PROTO_remove_node(struct ddl * list , int location){
	// find the node
	DDLNode past;
	DDLNode curnode = list->top;
	DDLNode next;
	if (location > list->size){
		//give error message
		printf("Provided Location out of bounds %d given where limit is %d!! remove_node FAILED!!\n", location, list->size);
		//return 
		return;
	}

	for (long i = 0 ; i < location ; i++ ){
		curnode = curnode->next;
	}
	past = curnode;
	curnode = curnode->next;
	next = curnode->next;


	// save it as a pointer variable
	// cut it from the list by setting prevs next and nexts prev to each other.. drop references 
	// free it using free()
	return;
};//close proto remove node

void PROTO_clear(struct ddl * list){
	//travese to bottom
	//delete from bottom up
	//unlink and free
	return;
};//close proto clear

void PROTO_add_at(struct ddl * list , int postoadd, _TYPE_ val){
	//go to position.
	//generate new node
	//change surounding node ptrs
	//increase size
	return;
};//close proto add at 

void PROTO_add(struct ddl * list , _TYPE_){
	//find end of list	
	//call ad at with this value
	PROTO_add_at(
	return;
	
}//close proto add

_TYPE_ PROTO_get_at(struct ddl * list , int location){
	//go to location
	//return value of data at that node
	//account for node not excisting
	return NULL:
};//close get at 

_TYPE_ PROTO_next( struct ddl * list  ){
	//use cursor to call get at 
	//grow cursor
	return NULL:
};//close proto next

//new_ddl
DDL * new_ddl(_TYPE_ top_val){
	
	/* 1. Make a new node */
	DDLNode * TOP = (DDLNode *) malloc(sizeof(DDLNode));			// allocate memory for node
	TOP->data = top_val;							// set default value
	
	/* 2. Make DDL */
	DDL * returnddl = (DDL*) malloc(sizeof(DDL));				//allocate memory for the ddl we will later return
	returnddl->size = 1;
	returnddl->cursor = 0;
	returnddl->top = &TOP;

	/* 3. Set function pointer to corisponding prototypes */
	returnddl->remove_node = PROTO_remove_node;
	returnddl->clear = PROTO_clear;
	returnddl->add_at = PROTO_add_at;
	returnddl->add = PROTO_add;
	returnddl->get_at = PROTO_get_at;
	returnddl->next = PROTO_next;

	/* 4. Return new DDL */
	return returnddl;
};

